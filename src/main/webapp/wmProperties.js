var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "TestVCS1",
  "homePage" : "Main",
  "name" : "TestVCS1",
  "platformType" : "WEB",
  "securityEnabled" : "false",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};